=============================
 Murano Client Release Notes
=============================

.. toctree::
   :maxdepth: 2

   unreleased
   zed
   yoga
   xena
   wallaby
   victoria
   ussuri
   train
   stein
   rocky
   queens
   pike
   ocata
   newton
   mitaka
   liberty
